package com.company;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

public class GetStat {
    public GetStat(long player_id) throws IOException, ParseException {
        JSONParser jsonParser = new JSONParser();
        getMatchesStat(jsonParser, player_id);

    }

    private StringBuffer getResponse(String url) throws IOException{
        URL obj = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) obj.openConnection();

        connection.setRequestMethod("GET");

        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();


        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }

        in.close();

        return response;
    }

    private void getMatchesStat(JSONParser jsonParser, long player_id) throws ParseException, IOException {
        String url = "https://api.opendota.com/api/players/" + player_id +"/matches";
        StringBuffer response = getResponse(url);
        JSONArray matches = (JSONArray) jsonParser.parse(response.toString());
        Iterator i = matches.iterator();

        try {
            System.out.println(matches.get(0));
            JSONObject m = (JSONObject) matches.get(0);

            Date currentTime = new Date((long)m.get("start_time")*1000L);
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            String dateString = formatter.format(currentTime);
            System.out.println("Последняя игра " + dateString);
        }
        catch (IndexOutOfBoundsException e){
            System.out.println("У игрока нет игр");
        }

        long skill=0;
        int counter = 0;

        while (i.hasNext() && counter < 20) {
            JSONObject match = (JSONObject) i.next();
            try {
                skill += (long) match.get("skill");
            }
            catch (NullPointerException e){
                continue;
            }
            counter++;
        }
        System.out.println("Средний уровень " + (double)skill/20);
    }
}
